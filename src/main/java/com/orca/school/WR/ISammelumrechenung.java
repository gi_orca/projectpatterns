package com.orca.school.WR;

/**
 * Interface for doing Bulk converion
 */
public interface ISammelumrechenung {

    /**
     * Converts an array of doubles into given currency
     * @param betraege used to pass the amounts in
     * @param variante used to express the type of conversion
     * @return all amounts summed up and converted into a single value
     */
    double sammelumrechnen(double[] betraege, String variante);
}

