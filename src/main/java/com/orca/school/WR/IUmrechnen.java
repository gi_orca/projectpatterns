package com.orca.school.WR;

/**
 * Interface to have a Currency conversion
 */
public interface IUmrechnen {

    /**
     * method to convert currency
     * @param variante used to express the type of conversion
     * @param betrag the amount to convert
     * @return the resulting amount in the target currency
     */
    double umrechnen(String variante, double betrag);
}
