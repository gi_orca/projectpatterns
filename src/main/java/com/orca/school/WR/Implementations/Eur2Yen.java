package com.orca.school.WR.Implementations;

import com.orca.school.WR.WR;

public class Eur2Yen extends WR {
    private String variant;

    public Eur2Yen() {
        this.setup();
    }

    Eur2Yen(WR successor){
        this.setup();
        this.setSuccessor(successor);
    }

    /**
     * used to set the specific parameters changes are to be made here
     */
    private void setup(){
        this.conversionFactor = 126.46;
        this.variant =  "EUR2YEN";
        this.setSourceCurrency("EUR");
        this.setDestinationCurrency("YEN");
    }
    @Override
    public double getConversionFactor() {
        return this.conversionFactor;
    }

    @Override
    public String getVariant() {
        return variant;
    }
}
