package com.orca.school.WR.Implementations;

import com.orca.school.WR.WR;

public class Eur2Dollar extends WR {
    private String variant;

    public Eur2Dollar() {
        this.setup();
    }

    Eur2Dollar(WR successor){
        this.setup();
        this.setSuccessor(successor);
    }

    /**
     * used to set the specific parameters changes are to be made here
     */
    private void setup(){
        this.conversionFactor = 1.14;
        this.variant =  "EUR2USD";
        this.setSourceCurrency("EUR");
        this.setDestinationCurrency("USD");
    }
    @Override
    public double getConversionFactor() {
        return this.conversionFactor;
    }

    @Override
    public String getVariant() {
        return variant;
    }
}
