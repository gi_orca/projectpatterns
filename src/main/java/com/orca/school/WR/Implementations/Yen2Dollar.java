package com.orca.school.WR.Implementations;

import com.orca.school.WR.WR;

public class Yen2Dollar extends WR {
    private String variant;

    public Yen2Dollar() {
        this.setup();
    }

    Yen2Dollar(WR successor){
        this.setup();
        this.setSuccessor(successor);
    }

    /**
     * used to set the specific parameters changes are to be made here
     */
    private void setup(){
        this.conversionFactor = 0.008994;
        this.variant =  "YEN2USD";
        this.setSourceCurrency("YEN");
        this.setDestinationCurrency("USD");
    }
    @Override
    public double getConversionFactor() {
        return this.conversionFactor;
    }

    @Override
    public String getVariant() {
        return variant;
    }
}
