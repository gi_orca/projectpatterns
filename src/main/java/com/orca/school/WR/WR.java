package com.orca.school.WR;

/**
 * Prototype of a currency conversion calculator
 * additonal conversions can be added by adding the
 * fitting WR implementation via {@link #addSuccessor(WR)}
 * to the chain(Chain of responsibility)
 */
public abstract class WR implements IUmrechnen {
    private WR successor;
    private String sourceCurrency;
    private String destinationCurrency;
    protected double conversionFactor;

    /**
     * Method to convert if can't find any class to handle Request returns 0
     * @param variante used to express the desired type of conversion
     * @param betrag the amount to convert
     * @return the converted amount
     */
    public double umrechnen(String variante, double betrag){
        double result = 0;
        //Template Hook here
        if(variante.equals(this.getVariant())) {
            result = betrag * this.getConversionFactor(); //and here again
        }
        else if (successor!=null) {
            result =successor.umrechnen(variante, betrag);
        }
        return result;
    }

    /**
     * Use this to add a new Calculator to the Chain of Responsibility appended at the end
     * @param successor Calculator to add
     */
    public void addSuccessor(WR successor){
        WR next = this;
        while (next.hasNext()){
            next = next.getSuccesssor();
        }
        next.setSuccessor(successor);
    }

    /**
     * removes the last Element of the Chain of Responsibility
     */
    public void removeSuccessor(){
        WR prev= this;
        WR next = prev;
        while (next.hasNext()){
            prev= next;
            next=prev.getSuccesssor();
        }
        prev.setSuccessor(null);
    }

    /**
     * Get the Source currency
     * @return String abbreviation of the source currency
     */
    public String getSourceCurrency() {
        return sourceCurrency;
    }

    /**
     * Gets the currency that its converted to
     * @return String abbreviation of the destination currency
     */
    public String getDestinationCurrency() {
        return destinationCurrency;
    }

    public WR getSuccesssor() {
        return this.successor;
    }

    public abstract String getVariant();

    protected void setSuccessor(WR successor) {
        this.successor=successor;
    }

    public abstract double getConversionFactor();

    /**
     * Helper Function to determine if the current Calculator is the last in the chain of Responsibility
     * @return boolean if  not last
     */
    private boolean hasNext() {
        boolean succ = false;
        if (this.successor != null){
            succ = true;
        }
        return succ;
    }

    protected void setSourceCurrency(String sourceCurrency) {
        this.sourceCurrency = sourceCurrency;
    }

    protected void setDestinationCurrency(String destinationCurrency) {
        this.destinationCurrency = destinationCurrency;
    }

    /**
     * Shows all useable variants in a chain
     * @return a String with Variants present in Chain
     */
    public String describe(){
        StringBuilder sb = new StringBuilder("'"+this.getVariant()+"'");
        WR next = this;
        while (next.hasNext()) {
            next = this.getSuccesssor();
            sb.append(", '");
            sb.append(next.getVariant());
            sb.append('\'');
        }
        return sb.toString();
    }
}
