package com.orca.school.WR.Decorators;

import com.orca.school.WR.WR;

/**
 * Decorates a WR object, adding a console output to the {@link #umrechnen(String, double)}  method
 */
public class LoggingDecorator extends WrDecorator {

    public LoggingDecorator(WR decoratee){
         super(decoratee);
    }


    /**
     * executes the {@link WR#umrechnen(String, double)} method of the underlying WR object
     * and additionally logs to console
     * @param variante used to express the type of conversion
     * @param betrag the amount to convert
     * @return the return value of the underlying WR object
     */
    @Override
    public double umrechnen(String variante, double betrag){
        // do Decorator things here
        System.out.println("Umrechnungsvorgang, "+ variante+ " mit dem Betrag "+ betrag);
        //
        return this.decorated.umrechnen(variante, betrag);
    }
}
