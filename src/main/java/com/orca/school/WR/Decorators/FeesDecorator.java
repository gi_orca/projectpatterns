package com.orca.school.WR.Decorators;

import com.orca.school.WR.WR;

/**
 * Decorates a WR object to subtract a given fee before doing calculation
 * can be used in a conversion chain
 */
public class FeesDecorator extends WrDecorator {
    private double fee;

    /**
     *
     * @param decorated the WR object to be decorated
     * @param fee %fee to be subtracted
     */
    public FeesDecorator(WR decorated, double fee){
        super(decorated);
        this.fee = fee;
    }

    /**
     * subtracts the fee before passing the request to its decorated object
     * @param variante used to express the desired type of conversion
     * @param betrag the amount to convert
     * @return the converted amount after fees were deducted
     */
    @Override
    public double umrechnen(String variante, double betrag) {
        //do your fee calculation here
        double reduced = betrag*(1.0-this.fee);
        return decorated.umrechnen(variante, reduced);
    }
}
