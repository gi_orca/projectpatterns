package com.orca.school.WR.Decorators;

import com.orca.school.WR.WR;

public abstract class WrDecorator extends WR {
    protected WR decorated;

    public WrDecorator(WR decorated){
        this.decorated = decorated;
    }

    @Override
    public String getVariant() {
        return this.decorated.getVariant();
    }

    @Override
    public double getConversionFactor() {
        return this.decorated.getConversionFactor();
    }

    /**
     *
     * @param variante used to express the desired type of conversion
     * @param betrag the amount to convert
     * @return the converted amount
     */
    @Override
    public abstract double umrechnen(String variante, double betrag);
}
