package com.orca.school;

import com.orca.school.WR.Decorators.FeesDecorator;
import com.orca.school.WR.Decorators.LoggingDecorator;
import com.orca.school.WR.Implementations.Eur2Dollar;
import com.orca.school.WR.Implementations.Eur2Yen;
import com.orca.school.WR.WR;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        WR rechner = new Eur2Yen();
        rechner.addSuccessor(new Eur2Dollar());
        rechner = new LoggingDecorator(rechner);
        rechner = new FeesDecorator(rechner, 0.005);
        System.out.println(rechner.umrechnen("EUR2USD", 1));
        System.out.println( rechner.umrechnen("EUR2YEN", 1) );
        System.out.println(rechner.describe());
    }
}
