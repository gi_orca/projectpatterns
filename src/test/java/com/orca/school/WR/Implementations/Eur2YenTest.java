package com.orca.school.WR.Implementations;

import com.orca.school.WR.WR;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Eur2YenTest {
private Eur2Yen calc;
    @BeforeEach
void setup(){
    this.calc = new Eur2Yen();
}
    @Test
    void umrechnen() {
        assertEquals(126.46, calc.umrechnen("EUR2YEN",1), "Eins umrechnen");
        assertEquals(0, calc.umrechnen("",0), "Null Betrag umrechnen");
    }

    @Test
    void addSuccessor() {
        WR test = new Eur2Yen();
        WR test2 = new Eur2Yen();
        calc.addSuccessor(test);
        calc.addSuccessor(test2);
        assertEquals(test, calc.getSuccesssor(), "Element added to chain");
        assertEquals(test2, test.getSuccesssor(), "Test 2nd Element was appended as well");
    }

    @Test
    void removeSuccessor() {
        assertNull(calc.getSuccesssor());
        WR test = new Eur2Yen();
        WR test2 = new Eur2Yen();
        calc.addSuccessor(test);
        calc.addSuccessor(test2);
        calc.removeSuccessor();
        assertEquals(test, calc.getSuccesssor());
        calc.removeSuccessor();
        assertNull(calc.getSuccesssor());

    }

    @Test
    void getVariant() {
        assertEquals("EUR2YEN", calc.getVariant());
    }

}