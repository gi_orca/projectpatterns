package com.orca.school.WR.Implementations;

import com.orca.school.WR.WR;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Eur2DollarTest {
private WR calc;
    @BeforeEach
    void setUp() {
        this.calc = new Eur2Dollar();
    }

    @Test
    void umrechnen() {
        double expected = calc.umrechnen("EUR2USD", 1);
        assertEquals(1.14, expected);
        double shouldBeNull = calc.umrechnen("", 1);
        assertEquals(0, shouldBeNull);
    }

    @Test
    void addSuccessor() {
        WR test = new Eur2Dollar();
        WR test2 = new Eur2Dollar();
        calc.addSuccessor(test);
        calc.addSuccessor(test2);
        assertEquals(test, calc.getSuccesssor(), "Element added to chain");
        assertEquals(test2, test.getSuccesssor(), "Test 2nd Element was appended as well");
    }

    @Test
    void removeSuccessor() {
        assertNull(calc.getSuccesssor());
        WR test = new Eur2Dollar();
        WR test2 = new Eur2Dollar();
        calc.addSuccessor(test);
        calc.addSuccessor(test2);
        calc.removeSuccessor();
        assertEquals(test, calc.getSuccesssor());
        calc.removeSuccessor();
        assertNull(calc.getSuccesssor());
    }

    @Test
    void getVariant() {
        assertEquals("EUR2USD", calc.getVariant());
    }
}