package com.orca.school.WR.Decorators;

import com.orca.school.WR.Decorators.LoggingDecorator;
import com.orca.school.WR.Implementations.Eur2Dollar;
import com.orca.school.WR.Implementations.Eur2Yen;
import com.orca.school.WR.WR;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LoggingDecoratorTest {
    private WR test;
    private WR calc;
    @BeforeEach
    void setUp() {
        this.calc = new Eur2Yen();
        this.test = new LoggingDecorator(calc);
    }

    @Test
    void umrechnen() {
        double expected = calc.umrechnen("EUR2YEN", 1);
        double actual = test.umrechnen("EUR2YEN", 1);
        assertEquals(expected, actual );
    }

    @Test
    void getVariant() {
        String expected = calc.getVariant();
        String actual = test.getVariant();
        assertEquals(expected, actual);
    }

    @Test
    void getConversionFactor() {
        double expected = calc.getConversionFactor();
        double actual = test.getConversionFactor();
        assertEquals(expected, actual);
    }


    @Test
    void addSuccessor() {
        WR anotherCalc = new Eur2Dollar();
        assertEquals(0, test.umrechnen("EUR2USD", 1));
        calc.addSuccessor(anotherCalc);
        double expected = anotherCalc.umrechnen("EUR2USD", 1);
        double actual = test.umrechnen("EUR2USD", 1);
        assertEquals(expected, actual);
    }
}